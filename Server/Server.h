#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <vector>
#include <queue>
#include <mutex>

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	SOCKET _serverSocket;
	std::vector<std::string> _activeUsers;

	//The global messages are constructed this way:
	//Author's Name#Message Reciver#Message
	std::queue<std::string> _globalMessages;
	std::mutex _queueLock;
	void writeToFile();
	void accept();
	void clientHandler(SOCKET clientSocket);

	std::string returnUsers() const;
	std::string loginPart(SOCKET clientSocket);
	std::string getFileName(std::string firstUsername, std::string secondUsername);
	std::string getFileConversationContent(std::string firstUsername, std::string secondUsername);


	   
};
