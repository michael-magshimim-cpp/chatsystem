#include "Helper.h"
#include "Server.h"

#include <thread>
#include <exception>
#include <iostream>
#include <string.h>
#include <fstream>


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - creating socket failed");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	std::thread writeToFileThread(&Server::writeToFile, this);
	writeToFileThread.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(this->_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// Creating the thread that will communicate with the user
	std::thread td(&Server::clientHandler,this,client_socket);
	td.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		std::string username = "";
		std::string secondUsername = "";
		std::string newMessage = "";
		int packetCode = 0;			//Packet id
		int length = 0;				//Length of a certain field
		
		username = this->loginPart(clientSocket);
				
		Helper::send_update_message_to_client(clientSocket,"", "", returnUsers(), username);//Login packet that only return the users
		
		//Login part done, now the conversation part
		while (true)
		{
			//Getting all the content 
			packetCode = Helper::getIntPartFromSocket(clientSocket, 3, username);
			
			int secondUserLength = Helper::getIntPartFromSocket(clientSocket, 2, username);
			std::string secondUsername = Helper::getStringPartFromSocket(clientSocket, secondUserLength, username);
			int newMessageLength = Helper::getIntPartFromSocket(clientSocket, 5, username);
			std::string newMessage = Helper::getStringPartFromSocket(clientSocket, newMessageLength, username);

			if (secondUserLength == 0)//If the user want no second user and no new message
				Helper::send_update_message_to_client(clientSocket, "", "", this->returnUsers(), username);//Just return all active users
			else
			{
				
				if (newMessageLength == 0)//User wants chat content but no new message
					Helper::send_update_message_to_client(clientSocket, this->getFileConversationContent(username, secondUsername), secondUsername, this->returnUsers(), username);
				else
				{
					std::cout << "New message: \nAUTHOR: " << username << "\nReciever: " << secondUsername << std::endl;
					{
						std::lock_guard<std::mutex> lock(this->_queueLock);
						//Push the message like that: author#reciever#message
						this->_globalMessages.push(username + "#" + secondUsername + "#" + newMessage);
					}

					std::string updatedMessage = "&MAGSH_MESSAGE&&Author&" + username + "&DATA&" + newMessage;
					std::string fileContent = this->getFileConversationContent(username, secondUsername) + updatedMessage, secondUsername;

					Helper::send_update_message_to_client(clientSocket, fileContent, secondUsername,this->returnUsers(), username);
				}
				std::cout << username << ": " << packetCode << secondUsername;
			}
		}
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		std::string username = e.what();
		username = username.substr(0, username.find("#"));

		std::vector<std::string>::iterator name = std::find(this->_activeUsers.begin(),this->_activeUsers.end(),username);
		if (name != this->_activeUsers.end())
			this->_activeUsers.erase(name);
		std::cout << "User Disconnected: " << e.what() << std::endl;
	}
}

std::string Server::returnUsers() const
{
	std::string result = "";
	for (unsigned int i = 0; i < this->_activeUsers.size();i++)
	{
		result += this->_activeUsers[i];
		result += "&";
	}
	//Delete the last &
	if(result != "")
		result.pop_back();
	return result;
}

std::string Server::loginPart(SOCKET clientSocket)
{
	std::string username = "";
	int packetCode = 0;			//Packet id
	int length = 0;				//Length of a certain field

	//Getting the code
	packetCode = Helper::getIntPartFromSocket(clientSocket, 3, "");
	std::cout << packetCode << " ";
	length = Helper::getIntPartFromSocket(clientSocket, 2, "");
	std::cout << length << " ";

	username = Helper::getStringPartFromSocket(clientSocket, length, "");
	this->_activeUsers.push_back(username);
	std::cout << username << std::endl;
	return username;
}

void Server::writeToFile()
{
	while (true)
	{
		std::string messageQueue = "";//The current message in the queue
		std::string finalFileMessage = "";
		while(!this->_globalMessages.empty())
		{
			{
				std::lock_guard<std::mutex> lock(this->_queueLock);
				messageQueue = this->_globalMessages.front();
				this->_globalMessages.pop();//Going to the next message
			}
			std::string firstUsername = messageQueue; //Also the author  
			std::string secondUsername = messageQueue;//Also the receiver

			firstUsername = messageQueue.substr(0, messageQueue.find("#"));
			messageQueue = messageQueue.substr(messageQueue.find("#") + 1,messageQueue.size() - messageQueue.find("#") + 1);

			secondUsername = messageQueue.substr(0, messageQueue.find("#"));
			messageQueue = messageQueue.substr(messageQueue.find("#") + 1, messageQueue.size() - messageQueue.find("#") + 1);

			std::string txtFileName = this->getFileName(firstUsername,secondUsername);
			finalFileMessage = "&MAGSH_MESSAGE&&Author&" + firstUsername + "&DATA&" + messageQueue;
			
			std::ofstream file(txtFileName, std::ios_base::app);

			bool wroteToFile = false;
			do
			{
				if (file.is_open())
				{
					file << finalFileMessage;
					file.close();
					wroteToFile = true;
				}
				else
					file.open(txtFileName);
			} while(!wroteToFile);
		}
	}
}

std::string Server::getFileName(std::string firstUsername, std::string secondUsername)
{
	std::string txtFileName = "";
	if (firstUsername.compare(secondUsername) < 0)
		txtFileName = secondUsername + "&" + firstUsername;
	else
		txtFileName = firstUsername + "&" + secondUsername;
	txtFileName += ".txt";
	return txtFileName;
}

std::string Server::getFileConversationContent(std::string firstUsername, std::string secondUsername)
{
	std::string txtFileName = this->getFileName(firstUsername,secondUsername);
	std::string content = "";
	std::string line = "";

	std::ifstream file(txtFileName);

	if (file.is_open())
	{
		while (std::getline(file, line))
			content += line;
		file.close();
	}
	std::cout << content << std::endl;
	return content;
}
